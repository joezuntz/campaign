Milestone for Y1 shear-shear work

- read a list of runs from a file
- if any of them are new, create them
- set any of them required running on fornax
- check the status of running jobs
- print out a nice status table
- update a job to make a second version of it
- postprocess everything asynchronously and organize results
- create summary tables in latex
