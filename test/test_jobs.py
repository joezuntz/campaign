from campaign.jobs import PortableBatchSystemJobManager, fornaxTemplate, fornaxCoresPerNode, fornaxSetup
from campaign.jobs import LocalLauncher
import os
import tempfile
import time

def get_hostname():
    return os.popen("hostname").read().strip()


def test_pbs():
    #This is system-specific - can only really test on one system
    if get_hostname()!="fornax.jb.man.ac.uk":
        return

    manager = PortableBatchSystemJobManager(fornaxSetup, fornaxTemplate, fornaxCoresPerNode)

    working_directory = tempfile.mkdtemp(dir=os.getcwd())
    name = "TEST"
    command_line = """ python -c 'import mpi4py.MPI;print "Rank=",mpi4py.MPI.COMM_WORLD.Get_rank()' """
    nodes = 1
    print
    print working_directory
    result=manager.launch(working_directory, name, command_line, nodes, 1.0/30)
    print result
    print

def test_local():
    launcher = LocalLauncher(2)
    jobs = []
    for i in xrange(5):
        cmd = "sleep {}; echo Just slept for {} seconds".format(2*i, 2*i)
        job = launcher.launch("/", "job-{}".format(i), cmd)
        jobs.append(job)
    t0 = time.time()
    while jobs and time.time()-t0<20:
        for job in jobs[:]:
            if job.poll() is not None:
                status, output = job.results()
                print "JOB COMPLETE", job.name, status, output.strip()
                jobs.remove(job)
        launcher.update()

    if time.time()-t0>=20:
        raise ValueError("Job timed out")

if __name__ == '__main__':
    test_local()