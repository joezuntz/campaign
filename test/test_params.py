from campaign.params import ParameterFile, Inifile
from cStringIO import StringIO
import tempfile
import os
import sys

def make_params_file():
    p = ParameterFile(None)
    p["xxx", "111"] = "zzz"
    p["xxx", "222"] = 14
    p["yyy", "111"] = 8.6
    p["yyy", "222"] = True
    p["yyy", "333"] = "48"
    filename = tempfile.mktemp(suffix='.ini')
    p.write(open(filename, "w"))

    return p, filename

def test_params():
    #Check that we can create an orphaned parameter file,
    #add some parameters to it, write it out in full or in 
    #part to a file, and then read it as a cosmosis
    #input file

    p,filename = make_params_file()
    #Test writing to file

    ini = Inifile(filename)
    assert ini.get("xxx", "111") == "zzz"
    assert ini.getint("xxx", "222") == 14
    assert ini.getfloat("yyy", "111") == 8.6
    assert ini.getboolean("yyy", "222") is True
    assert ini.get("yyy", "333") == "48"
    assert ini.get("xxx", "111") == "zzz"
    assert ini.gettyped("xxx", "222") == 14
    assert ini.gettyped("yyy", "111") == 8.6
    assert ini.gettyped("yyy", "222") is True


def test_inherit():
    p,filename = make_params_file()
    #Check that we can overwrite something in the file

    #Check that we can inherit okay.
    p2 = ParameterFile(filename)
    filename2 = tempfile.mktemp(suffix='.ini')

    #new parameters
    p2["xxx", "333"] = 100
    #overwritten parameter
    p2["yyy", "111"] = 9.5
    p2.write(open(filename2, "w"))


    ini = Inifile(filename2)
    #check base param
    assert ini.getint("xxx", "222") == 14
    #check new param
    assert ini.getint("xxx", "333") == 100
    #check overwritten param
    assert ini.getfloat("yyy", "111") == 9.5


