from PyQt5.QtCore import QDate, QDateTime, QRegExp, QSortFilterProxyModel, Qt, QTime
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QApplication, QCheckBox, QComboBox, QGridLayout
from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QLabel, QLineEdit
from PyQt5.QtWidgets import QTreeView, QVBoxLayout, QWidget
import campaign
from campaign import Campaign, Run
import campaign.status
import campaign.errors



class RunModel(object):
    def __init__(self, run):
        self.run = run

    def adapt(self, model):
        info = self.run.info()
        status_index = Run.INFO_FIELDS.index("Status")
        info = list(info)
        status = info[status_index]
        info[status_index] = campaign.status.STATUS_ICON[status]
        row = [QStandardItem(unicode(entry)) for entry in info]
        model.appendRow(row)



class CampaignWindow(QWidget):
    def __init__(self, campaign):
        super(CampaignWindow, self).__init__()
        self.campaign = campaign

        self.sourceGroupBox = QGroupBox("Data")

        self.sourceView = QTreeView()
        self.sourceView.setRootIsDecorated(False)
        self.sourceView.setAlternatingRowColors(True)
        self.sourceView.setModel(createModel(self, self.campaign))


        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.sourceView)
        self.setLayout(mainLayout)

        self.setWindowTitle("CosmoSIS Campaign")
        self.resize(800, 450)


def createModel(parent, campaign):
    table_model = QStandardItemModel(0, len(Run.INFO_FIELDS), parent)
    for i, param in enumerate(Run.INFO_FIELDS):
        table_model.setHeaderData(i, Qt.Horizontal, param)
    for run in campaign.runs.values():
        run_model = RunModel(run)
        run_model.adapt(table_model)
    return table_model

def main():
    import sys

    app = QApplication(sys.argv)
    campaign=Campaign.load("/Users/jaz/src/campaign/campaigns")
    window = CampaignWindow(campaign)
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()