from .names import *
from cosmosis.runtime.config import Inifile, CosmosisConfigurationError
import os
import logging
import collections
import contextlib
import tempfile
from .log import NOTE, logger


DEFAULT_PARAMETERS = {
    ('pipeline', 'values'): VALUES_FILENAME,
    ('pipeline', 'priors'): PRIORS_FILENAME,
    ('test', 'save_dir'): TEST_SUBDIR,
    ('output', 'filename'): CHAIN_FILENAME,
    ('output', 'format'): 'text',
}

VARIOUS_PARENTS = "Various Parents"

class ParameterFile(object):
    def __init__(self, base_path):
        self.base_path = base_path
        if base_path is None:
            self.abs_base_path = None
        else:
            self.abs_base_path = os.path.abspath(os.path.expandvars(base_path))
        self.changes = collections.OrderedDict()
        self._base_ini = None

    def __eq__(self, other):
        return (self.changes == other.changes) and (self.base_path == other.base_path)
    def __ne__(self, other):
        return (self.changes!=other.changes) or (self.base_path != other.base_path)

    def differences(self, other):
        diffs = {}
        keys_that_always_change = [("pipeline", "priors"), ("pipeline", "values"), ("output", "filename"), ("test", "save_dir"), ("output", "format")]
        for key,val1 in self.changes.items():
            #These ones will always change
            if key in keys_that_always_change:
                continue
            val2 = other.changes.get(key)
            if val1!=val2:
                diffs[key] = (val1, val2)
        #and the reverse too. will re-write but never mind.
        for key,val2 in other.changes.items():
            val1 = self.changes.get(key)
            if val1!=val2:
                diffs[key] = (val1, val2)
        return diffs


    @property
    def base_ini(self):
        if self._base_ini is None:
            self._base_ini = Inifile(self.abs_base_path)
        return self._base_ini

    @base_ini.deleter
    def base_ini(self):
        self._base_ini = None

    @classmethod
    def from_dict(cls, d, base_path=None):
        base_path = d.get('base_path', base_path)
        output = cls(base_path)
        for section,section_dict in d.items():
            if section == 'base_path': continue
            for name,val in section_dict.items():
                output[(section,name)] = val
        return output

    def parent_item(self, parameter):
        section,name=parameter
        return self.base_ini.get(section, name)

    @contextlib.contextmanager
    def temporary_value(self, section, name, value):
        old_value = self.changes.get((section,name))
        self[(section,name)] = value
        yield
        if old_value is None:
            del self[(section,name)]
        else:
            self[(section,name)] = old_value
    
    def items(self, section):
        d = collections.OrderedDict()
        if self.base_ini:
            for key,value in self.base_ini.items(section):
                d[key] = value
        for (s,key), value in self.changes.items():
            if s==section:
                d[key] = value
        return d


    def __setitem__(self, parameter, value):
        section, name = parameter
        self.changes[(section,name)] = value

    def __delitem__(self, parameter):
        del self.changes[parameter]

    def __getitem__(self, parameter):
        item = self.changes.get(parameter)
        if item is not None:
            return item
        item = self.parent_item(parameter)
        if item is not None:
            return item
        section,name=parameter
        raise KeyError("No parameter {0} in section {1}".format(name, section))

    def __contains__(self, parameter):
        return parameter in self.changes

    def copy(self):
        result = self.__class__(self.base_path)
        result.changes.update(self.changes)
        return result


    def write(self, stream):
        """
        Write the parameter file in abbreviated form, where the base ini
        file is included in the parent by reference - the cosmosis %include 
        notation is used at the top of the file to tell cosmosis to include it
        first.
        """
        #First the include statement, to contain the base
        #This may be blank - if so, ignore
        if self.base_path is not None:
            stream.write("%include {0}\n\n".format(self.base_path))
        #We use the standard ini file mechanism to save the
        #parameters. Because we have already just included the
        #parent base file we do not need to include it again-
        #hence the "None" argument.
        ini = Inifile(None, override=self.changes)
        ini.write(stream)

    @classmethod
    def read(cls, filename):
        #Read base path
        f = open(filename)
        line = f.readline().strip()
        f.close()
        if line.startswith('%include'):
            base_path = line.split(None,1)[1]
        else:
            base_path = None
        

        #Now read the contents
        ini = Inifile(None)
        ini.no_expand_vars = True
        ini.no_expand_includes = True
        ini.read(filename)
        changes = collections.OrderedDict()
        for name,value in ini.defaults().items():
            changes[("DEFAULT", name)] = value
        for section in ini.sections():
            for name,value in ini.items(section,defaults=False,raw=True):
                changes[(section,name)] = value


        p = cls(base_path)
        p.changes = changes
        return p


    def write_full(self, stream):
        """
        Write the parameter file in full form, by loading the base ini file
        and writing it out in full. This would typically be used for debugging.
        """
        #Use the standard inifile mechanism to load the file
        #and apply the changes
        ini = Inifile(self.abs_base_path, override=self.changes)
        ini.write(stream)


    def normalize(self):
        with tempfile.NamedTemporaryFile() as f:
            self.write(f)
            f.flush()
            tmp = self.read(f.name)
        self.changes = tmp.changes


class RunParameters(object):

    def __init__(self, params, values, priors):
        #ParameterFile instances
        self.params = params
        self.values = values
        self.priors = priors

    @property
    def parent_dir(self):
        def getdir(p):
            base=p.base_path
            if base is None:
                return None
            else:
                return os.path.dirname(base)

        path1, path2, path3 = [getdir(p) for p in self.params, self.values, self.priors]

        if path1==path2 and (path1==path3 or path3==None): #priors are optional so path3 may be none
            return path1
        else:
            return VARIOUS_PARENTS

    @parent_dir.setter
    def parent_dir(self, parent_dir):
        if parent_dir is None:
            self.params.base_path = None
            self.values.base_path = None
            self.priors.base_path = None
        else:
            self.params.base_path = os.path.join(parent_dir, PARAM_FILENAME)
            self.values.base_path = os.path.join(parent_dir, VALUES_FILENAME)
            self.priors.base_path = os.path.join(parent_dir, PRIORS_FILENAME)

    def normalize(self):
        self.params.normalize()
        self.values.normalize()
        self.priors.normalize()

    @classmethod
    def read(cls, directory):
        params = ParameterFile.read(os.path.join(directory, PARAM_FILENAME))
        values = ParameterFile.read(os.path.join(directory, VALUES_FILENAME))
        priors = ParameterFile.read(os.path.join(directory, PRIORS_FILENAME))
        return cls(params, values, priors)


    def __eq__(self, other):
        param_diffs = self.params.differences(other.params)
        value_diffs = self.values.differences(other.values)
        prior_diffs = self.priors.differences(other.priors)

        #remove the default ones which get written always - we ignore
        #these
        for key in DEFAULT_PARAMETERS:
            param_diffs.pop(key, None)

        if param_diffs:
            logger.log(logging.DEBUG, "Parameter file has changed (%s).",
                param_diffs.keys())
            return False
        if value_diffs:
            logger.log(logging.DEBUG, "Values file has changed (%s).",
                value_diffs.keys()
                )
            return False
        if prior_diffs:
            logger.log(logging.DEBUG, "Priors file has changed (%s).",
                prior_diffs.keys())
            return False
        return True

    def copy(self):
        return RunParameters(self.params.copy(), self.values.copy(), self.priors.copy())

    def set_paths(self):
        for (section,key), value in DEFAULT_PARAMETERS.items():
            self.params[section, key] = value
 

    def write(self, directory):
        self.set_paths()

        filename = os.path.join(directory, PARAM_FILENAME)
        self.params.write(open(filename, "w"))

        filename = os.path.join(directory, VALUES_FILENAME)
        self.values.write(open(filename, "w"))

        filename = os.path.join(directory, PRIORS_FILENAME)
        self.priors.write(open(filename, "w"))

    @classmethod
    def from_dict(cls, d):
        base_params = d.get('base_params')
        base_values = d.get('base_values')
        base_priors = d.get('base_priors')
        params = ParameterFile.from_dict(d.get('params', {}), base_params)
        values = ParameterFile.from_dict(d.get('values', {}), base_values)
        priors = ParameterFile.from_dict(d.get('priors', {}), base_priors)
        return cls(params, values, priors)

    @classmethod
    def from_ini(cls, base_dir, params_path):
        params_name = os.path.join(base_dir, params_path)
        params = Inifile(params_name)

        values_name = params.get("pipeline", "values")
        values_name = os.path.join(base_dir, values_name)

        #priors file is optional
        priors_name = params.get("pipeline", "priors", default="")
        if priors_name:
            priors_name = os.path.join(base_dir, priors_name)
        else:
            priors_name = None

        params = ParameterFile(params_name)
        values = ParameterFile(values_name)
        priors = ParameterFile(priors_name)

        return cls(params, values, priors)
