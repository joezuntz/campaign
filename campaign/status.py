#coding: utf-8

# Status values for a run
STATUS_NEW = "NEW"
STATUS_WAITING = "WAITING"
STATUS_PREPARED = "PREPARED"
STATUS_MARKED_INVALID = "MARKED_INVALID"
STATUS_RUNNING = "RUNNING"
STATUS_QUEUED = "QUEUED"
STATUS_COMPLETE = "COMPLETE"
STATUS_ERROR = "ERROR"
STATUS_SUPERSEDED = "SUPERSEDED"
STATUS_UNKNOWN = "UNKNOWN"
STATUS_BASE = "UMBRELLA"

STATUS_CODES = {
    "STATUS_NEW":STATUS_NEW,
    "STATUS_WAITING":STATUS_WAITING,
    "STATUS_PREPARED":STATUS_PREPARED,
    "STATUS_MARKED_INVALID":STATUS_MARKED_INVALID,
    "STATUS_RUNNING":STATUS_RUNNING,
    "STATUS_QUEUED":STATUS_QUEUED,
    "STATUS_COMPLETE":STATUS_COMPLETE,
    "STATUS_ERROR":STATUS_ERROR,
    "STATUS_SUPERSEDED":STATUS_SUPERSEDED,
    "STATUS_UNKNOWN":STATUS_UNKNOWN,
    "STATUS_BASE":STATUS_BASE,
}


STATUS_ICON = {
    STATUS_NEW: u"",
    STATUS_QUEUED: u"\u270B",
    STATUS_PREPARED: u"\u2691",
    STATUS_MARKED_INVALID: u"\u2A02" ,
    STATUS_RUNNING: u"\u26A1",
    STATUS_WAITING: u"\u231B",
    STATUS_COMPLETE: u"\u2705",
    STATUS_ERROR: u"\u274C",
    STATUS_SUPERSEDED: u"\u2A01",
    STATUS_UNKNOWN: u"\u2753",
    STATUS_BASE: u"\u2602",
   
}

STATUS_NAMES = [code[len("STATUS")+1:] for code in STATUS_CODES]


if __name__ == '__main__':
    for name, icon in STATUS_ICON.items():
        print name, icon