#coding: utf-8
import collections
import os
from .log import NOTE, logger
from .run import Run
from .errors import WrongStatus, RunExists
from .status import *
from .params import RunParameters
from .names import *
from . import utils
# import cPickle
import yaml
import shutil
import subprocess
import itertools
import watchdog.events
import watchdog.observers
import collections


class FileChangeHandler(watchdog.events.FileSystemEventHandler):
    def __init__(self, campaign):
        super(FileChangeHandler, self).__init__()
        self.campaign=campaign

    def on_modified(self, event):
        if event.is_directory:
            return
        dirname, filename = os.path.split(event.src_path)
        if filename in ['params.ini', 'values.ini', 'priors.ini', 'notes.txt']:
            try:
                run_name = dirname.split(os.path.sep)[-1]
            except IndexError:
                return
            self.campaign.run_was_edited(run_name)



class Campaign(object):
    def __init__(self, campaign_directory, runs=None):
        self.campaign_directory = campaign_directory
        self.name = os.path.basename(os.path.normpath(self.campaign_directory))
        self.runs_subdir = os.path.join(campaign_directory, RUNS_SUBDIR)
        if not os.path.exists(self.runs_subdir):
            utils.mkdir(self.runs_subdir)
        if runs is None:
            self.runs = collections.OrderedDict()
        else:
            self.runs = collections.OrderedDict()
            for run in runs:
                self.runs[run.name] = run
                
        self.monitor_run_directory()
    
    def monitor_run_directory(self):
        event_handler = FileChangeHandler(self)
        self.observer = watchdog.observers.Observer()
        self.observer.schedule(event_handler, self.runs_subdir, recursive=True)
        self.observer.start()
        self.updated_runs = set()

    def run_was_edited(self, run_name):
        self.updated_runs.add(run_name)


    def process_updates(self):
        for run_name in list(self.updated_runs):
            run = self.runs.get(run_name)
            if run:
                logger.log(NOTE, "Run %s was updated",  run_name)
                run.synchronize()
            else:
                logger.log(NOTE, "A file was updated in dir '%s' but I do not know of any correspondingly named run yet.",  run_name)
            

        self.updated_runs = set()


    def update(self):
        for run in self.runs.values():
            run.update()

    @classmethod
    def create(cls, directory):
        return cls(directory, [])

    def stop(self, run_name):
        run = self.runs[run_name]
        run.stop()

    @classmethod
    def load(cls, directory):
        runs = []
        logger.log(NOTE, "Loading campaign from {}".format(directory))
        runs_subdir = os.path.join(directory, RUNS_SUBDIR)
        if os.path.exists(runs_subdir):
            for run_dir in os.listdir(runs_subdir):
                run_dir = os.path.join(runs_subdir, run_dir)
                if not os.path.isdir(run_dir):
                    continue
                try:
                    run = Run.read_directory(directory, run_dir)
                    runs.append(run)
                except IOError as error:
                    logger.error("Could not read possible run in %s.  Error was: %s", run_dir, error)
        else:
            logger.warning("Loading campaign from {}".format(directory))
            runs = []
        logger.log(NOTE, "Found %d active runs.",len(runs))
        campaign = cls(directory, runs=runs)
        return campaign


    def read_yaml(self, filename, dry_run=False):
        """Update runs based on the yaml file that defines runs"""
        #We have loaded a list of runs from file
        #Each of these runs can be in one of three states:
        #   New - this run does not exist in the class
        #   Old - this run exists unchanged in the class
        #   Changed - this run exists but has been updated in the yaml file
        desired_runs = utils.yaml_ordered_load(open(filename))

        new_runs = []
        changed_runs = []
        parents = []

        for name, data in desired_runs.items():
            if name.startswith("_"):
                continue
            #create the new run and compare to the old
            new_run, parent = Run.from_dict(self.campaign_directory, name, data, prepare=False)
            new_run.parameters.normalize()
            new_run.parameters.normalize()

            old_run = self.runs.get(name)
            # This is a new run
            if old_run is None:
                new_runs.append((new_run,parent))
            elif old_run.needs_rerun(new_run):
                #This is a changed run
                changed_runs.append((old_run, new_run, parent))

        all_runs = [p[0] for p in new_runs] + [p[1] for p in changed_runs] + self.runs.values()
        def assign_parent(run,parent):
            print "Looking for parent {} of run {}".format(parent,run)
            if parent is None:
                return
            for possible_parent in all_runs:
                if possible_parent.name==parent:
                    logger.log(NOTE, "Run %s is parent of new run %s",possible_parent.name,run)
                    run.parent = possible_parent
                    break
            else:
                #if for loop does not break
                raise ValueError(NOTE, "No parent %s found for run %s",parent, run)

        for run,parent in new_runs[:]:
            assign_parent(run,parent)
        for _, run, parent in changed_runs[:]:
            assign_parent(run,parent)

        #Now we have a list of new and changed runs.
        #The new runs are easy:
        for run,_ in new_runs:
            if dry_run:
                logger.log(NOTE, "(Dry-run) Would add new job %s", 
                    run.name)
            else:
                logger.log(NOTE, "Adding new run %s", 
                    run.name)
                self.runs[run.name] = run
                run.prepare()

        #The changed ones also simple enough - we need
        #to make sure the version number is one higher than the
        #existing one and then replace
        for old_run, new_run, _ in changed_runs:            
            if dry_run:
                logger.log(NOTE, "(Dry-run) Would update run %s due to changes", 
                    new_run.name)
            else:
                logger.log(NOTE, "Updating run %s due to changes", 
                    new_run.name)
                self.replace(old_run, new_run)
                new_run.prepare()

    def runs_with_status(self, status):
        for run in self.runs.values():
            if run.status == status:
                yield run

    def replace(self, old_run, new_run):
        trash_dir = self.trash_dir(old_run.name)
        old_run.archive(trash_dir)
        del old_run
        self.runs[new_run.name] = new_run


    def copy(self, existing_name, new_name):
        assert existing_name != new_name

        if new_name in self.runs:
            raise RunExists(new_name)

        run = self.runs[existing_name]
        run_copy = run.copy(new_name)
        self.runs[new_name] = run_copy

    def spawn(self, parent_name, child_names):
        try:
            parent = self.runs[parent_name]
        except KeyError:
            raise NoSuchRun("Run {} already exists".format(parent_name))

        for child_name in child_names:
            if child_name in self.runs:
                raise RunExists("Run {} already exists".format(child_name))

        for child_name in child_names:
            child_run = parent.spawn(child_name)
            self.runs[child_name] = child_run


    def launch(self, name):
        run = self.runs[name]
        run.launch()

    def quick_launch(self, name):
        run = self.runs[name]
        run.launch(test_first=False)

    def list(self):
        output = [run.info() for run in self.runs.values()]
        return output       

    def run_names(self):
        return [run.name for run in self.runs.values()]     

    def edit(self, run_name, filename):
        run = self.runs[run_name]
        #Check if run is in the wrong status - can only edit if 
        #it is not running or already run, unless we are only trying to edit the text notes.
        run.edit(filename)

    def delete(self, run_name):
        run = self.runs[run_name] 
        self.trash(run)
        del self.runs[run_name]

    def trash_dir(self, name):
        trash_dir_base = os.path.join(self.campaign_directory, TRASH_SUBDIR, name)
        i=0
        trash_dir = trash_dir_base + '.' + str(i)
        while os.path.exists(trash_dir):
            trash_dir = trash_dir_base + '.' + str(i)
            i+=1
        return trash_dir

    def trash(self, run):
        trash_dir = self.trash_dir(run.name)
        run.archive(trash_dir)

    def reset(self, run_name):
        run = self.runs[run_name]
        self.trash(run)
        run.status = STATUS_NEW
        #recreate it
        self.runs[run_name] = run
        run.prepare()

    def create(self, *run_names):
        runs = []
        for run_name in run_names:
            run, parent = Run.create_blank(self.campaign_directory, run_name, None)
            self.runs[run_name] = run
            runs.append(run)
        return runs

    def ingest(self, name, params, values, priors, text=""):
        if name in self.runs:
            raise RunExists("Run {} already exists".format(name))

        run = Run.ingest(self.campaign_directory, name, params, values, priors, text)
        self.runs[name] = run



    #COMPLETE
    #contains collections of jobs
    #can copy existing jobs
    #can make a new job version
    #can create new jobs from some kind of ini file
    #has a launcher for running them
    #can read a big list of jobs, see which ones have changed, etc.
    #displays jobs to user. 

    #TODO

    #understands job dependencies

