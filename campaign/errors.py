class RunExists(Exception):
    pass

class NoSuchRun(Exception):
    pass

class WrongStatus(Exception):
    pass

class LaunchError(Exception):
    pass

    
class UIError(Exception):
    pass
