import os
import sys
import cmd
import argparse
import logging
import collections
import humanize
import datetime
import glob
import readline
import traceback
import shlex
from . import jobs
from . import utils
from . import errors
from .campaign import Campaign
from .tabulate import tabulate
from .log import NOTE
from .status import *
from .errors import *
from .debug import enable_postmortem
import subprocess
import os

readline.set_completer_delims(' \t\n')



selecting_runs_doc = """

    Runs can be chosen by name:
        my_run_name
    Or by index:
        1
    You can specify more than one run:
        my_run_name 2 3 4
    And you can also specify multiple or ranges of runs:
        2-4,6,7
"""

class CampaignUI(cmd.Cmd):
    def __init__(self, campaign_directory):
        campaign_directory = os.path.abspath(campaign_directory)
        os.environ['CAMPAIGN_DIR'] = campaign_directory
        self.campaign_directory = campaign_directory
        self.done_preamble = False
        cmd.Cmd.__init__(self)

    @staticmethod
    def _is_first_arg(line, begidx):
        line, n = utils.lstrip_count(line)
        if not ' ' in line:
            #No spaces at all yet - return 
            return True
        begidx -= n
        print
        print begidx, line.index(' ')
        return begidx > line.index(' ')


    def _autocomplete_run_name(self, text, line, begidx, endidx):
        return [name for name in self.campaign.run_names() if name.startswith(text)]

    def _autocomplete_run_name_files(self, text, line, begidx, endidx, command):
        words = line.split()
        if (len(words)==2 and line[-1]!=' ') or (words==[command]):
            return self._autocomplete_run_name(text, line, begidx, endidx)
        else:
            try:
                run = self.parse_run(words[1])
                if len(words)>2:
                    start = words[-1]
                else:
                    start = ""
                files = self._autocomplete_files_in(run.run_directory(), start)
                return files
            except UIError:
                return []

    def _autocomplete_path_after_first_arg(self, text, line, begidx, endidx, command):
        words = line.split()
        if (len(words)==2 and line[-1]!=' ') or (words==[command]):
            return []
        pattern = "{}*".format(text)
        f =  glob.glob(pattern)
        return f


    def _autocomplete_path(self, text, line, begidx, endidx):
        pattern = "{}*".format(text)
        f =  glob.glob(pattern)
        return f

    def _autocomplete_files_in(self, dirname, start):
        pattern = "{}/{}*".format(dirname, start)
        f =  glob.glob(pattern)
        n = len(dirname)+1
        f = [g[n:] for g in f]
        return f



    def onecmd(self, *args, **kwargs):
        try:
            return cmd.Cmd.onecmd(self, *args, **kwargs)
        except UIError as error:
            logging.error(error) 
        except KeyboardInterrupt:
            pass
        except StandardError as error:
            logging.error("Unhandled exception:")
            traceback.print_exc()

    def preloop(self):
        #choose a directory and create a campaign.
        if not self.done_preamble:
            self.campaign = Campaign.load(self.campaign_directory)     
            self.prompt = "(Campaign) "
            self.do_list("")
            print("")
            self.load_history()
            self.done_preamble = True

    def cmdloop_with_keyboard_interrupt(self):
        #From StackOverflow 8813291
        done=False
        while not done:
            try:
                self.cmdloop()
                done=True
            except KeyboardInterrupt:
                sys.stdout.write("\n")  


    def load_history(self):
        try:
            import readline
            history = self.history_file()
            if os.path.exists(history):
                readline.read_history_file(history)
        except:
            pass

    def save_history(self):
        try:
            import readline
            history = self.history_file()
            history = os.path.join(self.campaign_directory, ".campaign_ui_history")
            readline.write_history_file(history)

        except ImportError:
            pass

    def history_file(self):
        return os.path.join(self.campaign_directory, ".campaign_ui_history")

    def postcmd(self, stop, line):
        if stop:
            return True
        self.campaign.process_updates()

    def emptyline(self):
        pass

    def postloop(self):
        self.save_history()

    def parse_filename(self, line):
        filename = line.strip()
        if not os.path.exists(filename):
            raise UIError("File {} does not exist".format(filename))
        if not os.path.isfile(filename):
            raise UIError("{} is not a file - probably a directory?".format(filename))
        if not os.access(filename, os.R_OK):
            raise UIError("{} is not accessible - probably a permissions problem.".format(filename))
        return filename

    def parse_filenames(self, line, blank=False):
        filenames = line.strip().split()
        if blank:
            filenames = [self.parse_filename(f) if filename!='-' else None
                        for f in filename]
        else:
            filenames = [self.parse_filename(f) for f in filename]            
        return filenames


    def parse_run_files(self, line, msg):
        words = line.split()
        if len(words)<2:
            logging.error("Syntax: {} run_name filename[s]".format(msg))
            return []
        run = self.parse_run(words[0])
        run_dir = run.run_directory()
        files = words[1:]
        paths = []
        for filename in files:
            path = os.path.join(run_dir, filename)
            paths.append(path)
        return paths

    def parse_run(self, line):
        name = line.strip()
        if name=='':
            raise UIError("Specify a run name (type 'list' to get a list of runs)")
        try:
            run = self.campaign.runs[name]
            return run
        except KeyError:
            raise UIError("Unknown run '{}'".format(name))

    def parse_runs(self, line):
        names = line.strip().split()
        if not names:
            raise UIError("Specify a run name (type 'list' to get a list of runs)")

        runs = []
        for name in names:
            if all(c in '0123456789,-' for c in name):
                indices = utils.parse_integers(name)
                for index in indices:
                    try:
                        run = self.campaign.runs.values()[index]
                        runs.append(run)
                    except IndexError:
                        raise UIError("Could not get run {}; out of range".format(index))
            else: #run with name
                runs.append(self.parse_run(name))
        return runs

    def print_table(self, data, header=()):
        """Print a table in a UI standard style. Pass in a list of rows and a header list."""
        print tabulate(data, header, tablefmt='fancy_grid', stralign='center', numalign='decimal').encode('utf-8')


    def file_info_table(self, dirname, recurse):
        data = self.file_info_table_sub(dirname, recurse)
        n = len(dirname)+1
        for row in data:
            row[0] = row[0][n:]
        return data

    def file_info_table_sub(self, dirname, recurse):
        data = []
        files = os.listdir(dirname)
        for filename in files:
            path = os.path.join(dirname, filename)
            if os.path.isdir(path) and recurse:
                data += self.file_info_table_sub(os.path.join(dirname, filename), recurse)
            elif os.path.isdir(path):
                st = os.stat(path)
                date = datetime.datetime.fromtimestamp(st.st_mtime)
                date = humanize.naturaltime(date)
                size = humanize.naturalsize(st.st_size)
                lines = "(dir)"
                data.append([path, date, size, lines])
            else:
                st = os.stat(path)
                date = datetime.datetime.fromtimestamp(st.st_mtime)
                date = humanize.naturaltime(date)
                size = humanize.naturalsize(st.st_size)
                lines = utils.line_count(path)
                data.append([path, date, size, lines])
        return data

    def directory_file_list(self, dirname, recurse):
        if not os.path.exists(dirname):
            logging.log(NOTE, "Run directory does not yet exist: %s", dirname)
            return

        files = os.listdir(dirname)
        header = ["Filename",  "Date", "Size", "Lines"]
        data = self.file_info_table(dirname, recurse)
        self.print_table(data, header)


    def do_launch_info(self, line):
        "Display the current job launcher. Currently just prints out the instance."
        print self.campaign.launcher

    def do_EOF(self, line):
        return True

    def do_read(self, line):
        """Load in a YAML file describing a collection of runs. Runs that are already in the 
        campaign will not be affected; new runs will be added, and runs that have been modified 
        compared to the version in the file will be updated and the old version invalidated.
        """
        filename = self.parse_filename(line)
        self.campaign.read_yaml(filename)
    complete_read = _autocomplete_path

    def do_peek(self, line):
        """Like the read command, but just a dry run - do not make any new runs
        but do describe what "read" would have done.
        """
        
        filename = self.parse_filename(line)
        self.campaign.read_yaml(filename, dry_run=True)
    complete_peek = _autocomplete_path



    def do_list(self, line):
        """Print a list of all the known runs, displaying various bits of info about them."""
        header = ['', 'Name',  'Parent', 'Status', 'Sampler', 'Chain length', 'Outputs', 'Job']
        table = []
        for i,info in enumerate(self.campaign.list()):
            (name, parent, status, sampler, lines, n_outputs, job) = info
            if lines==-1:
                lines=''
            status = STATUS_ICON.get(status, status)
            if parent is None:
                parent=""
            table.append((i, name,  parent, status, sampler, lines, n_outputs, job))
        self.print_table(table, header)

    def do_log(self, line):
        """Print out the log file from a run, if the job has been run."""
        run = self.parse_run(line)
        path = run.log_filename()
        if os.path.exists(path):
            logging.log(NOTE, path)
            for line in open(path):
                sys.stdout.write(line)
            sys.stdout.write("\n")
        else:
            logging.log(NOTE, "No log file %s (job may not have run yet)")
    complete_log = _autocomplete_run_name


    def do_error(self, line):
        """Do not run this command."""
        raise UIError("Please do not run this command again")

    def do_stop(self, line):
        """Kill the job for one or more running chains."""
        runs = self.parse_runs(line)
        names = [run.name for run in runs]
        for name in names:
            self.campaign.stop(name)
    complete_stop = _autocomplete_run_name

    def do_submit(self, line):
        """Start one or more chains running."""
        runs = self.parse_runs(line)
        names = [run.name for run in runs]
        for name in names:
            try:
                self.campaign.launch(name)
            except errors.WrongStatus:
                logging.error("This job is in the wrong state to run: {}".format(run.status))
                break
    complete_submit = _autocomplete_run_name

    def do_qsubmit(self, line):
        """Start one or more chains running, without testing them first"""        
        runs = self.parse_runs(line)
        names = [run.name for run in runs]
        for name in names:
            try:
                self.campaign.quick_launch(name)
            except errors.WrongStatus:
                logging.error("This job is in the wrong state to run: {}".format(run.status))
                break
    complete_qsubmit = _autocomplete_run_name

    def do_reset(self, line):
        """Reset one or more runs, archiving their chains and outputs"""
        runs = self.parse_runs(line)
        for run in runs:
            self.campaign.reset(run.name)

    complete_reset = _autocomplete_run_name

    def do_invalidate(self, line):
        """Mark one or more runs as invalidated to remind you later that they needs fixing"""
        runs = self.parse_runs(line)
        for run in runs:
            run.invalidate()

    complete_invalidate = _autocomplete_run_name


    def do_outputs(self, line):
        """List the files in a run's outputs directory, where postprocess outputs are put"""
        run = self.parse_run(line)
        outputs_dir = run.output_directory()
        self.directory_file_list(outputs_dir, True)
    complete_outputs = _autocomplete_run_name


    def do_ls(self, line):
        """
        With no argument, list all jobs.  With an argument, list the files that exist for that run
        without recursing into subdirectories.
        """
        if line.strip()=="":
            return self.do_list(line)
        run = self.parse_run(line)
        run_dir =run.run_directory()
        self.directory_file_list(run_dir, False)
    complete_ls = _autocomplete_run_name

    def do_lsl(self, line):
        """
        With no argument, list all jobs.  With an argument, list the files that exist for that run
        and do recurse into subdirectories.
        """
        if line.strip()=="":
            return self.do_list(line)
        run = self.parse_run(line)
        run_dir =run.run_directory()
        self.directory_file_list(run_dir, True)
    complete_lsl = _autocomplete_run_name


    def do_cat(self, line):
        """
        Syntax: cat run_name file_names
        print out the contents of the named files in the given run dir
        """
        for path in self.parse_run_files(line, 'cat'):
            if not os.path.exists(path):
                logging.error("File %s does not exist", path)
                continue
            for line in open(path):
                sys.stdout.write(line)
            sys.stdout.write("\n")

    def complete_cat(self, text, line, begidx, endidx):
        return self._autocomplete_run_name_files(text, line, begidx, endidx, "cat")


    def do_edit(self, line):
        """
        Syntax: edit run_name file_names
        Edit the named file in the given run dir with emacs no-window mode
        """
        words = line.split()
        try:
            run_name = words[0]
            filename = words[1]
        except IndexError:
            raise UIError("Syntax: edit run_name file_name")
            return
        try:
            self.campaign.edit(run_name, filename)
        except KeyError:
            raise UIError("Run {} does not exist".format(run_name))
        except WrongStatus as error:
            if error.message==STATUS_NEW:
                raise UIError("Please 'prepare' {} before editing files.".format(run_name))
            else:
                raise UIError("You cannot edit run {} as you have already launched it.  Use next or copy to make a new version.".format(run_name))
        except ValueError as error:
            logging.error(error)

    def complete_edit(self, text, line, begidx, endidx):
        return self._autocomplete_run_name_files(text, line, begidx, endidx, "edit")


    def do_update(self, line):
        """
        Re-read statuses of jobs from disc, and check for completed jobs
        """
        self.campaign.update()


    def do_test(self, line):
        """
        Test one or more runs with the test sampler
        """
        runs = self.parse_runs(line)
        for run in runs:
            try:
                run.test_synchronously()
            except errors.WrongStatus:
                logging.error("Run %s is in the wrong status: %s.  Maybe you need to reset it?",run.name,run.status)
    complete_test = _autocomplete_run_name

    complete_next = _autocomplete_run_name

    def do_print(self, line):
        """
        Print details of one or more runs
        """
        runs = self.parse_runs(line)
        for run in runs:
            table = run.detail()
            self.print_table(table)
            print
            print
    complete_print = _autocomplete_run_name

    def print_run_table(self, run_infos):
        header = ['', 'Name', 'Status', 'Sampler', 'Chain length', 'Outputs', 'Job']
        table = []
        for i,info in enumerate(run_infos):
            (name, status, sampler, lines, n_outputs, job) = info
            if lines==-1:
                lines=''
            status = STATUS_ICON.get(status, status)
            table.append((i, name, status, sampler, lines, n_outputs, job))
        self.print_table(table, header)

    def do_old(self, line):
        """
        Print details of old runs
        """
        self.print_run_table(self.campaign.list_old())

    def do_copy(self, line):
        """
        Copy a run to a new one.
        """
        words = line.split()
        if len(words)!=2:
            raise UIError("Syntax: copy  run_name  new_run_name")
        run_name, new_run_name = words
        self.campaign.copy(run_name, new_run_name)
    complete_copy = _autocomplete_run_name

    do_cp = do_copy
    complete_cp = complete_copy


    def do_spawn(self, line):
        """
        Spawn one or more runs from a base (parent) run, so that the 
        new run inherits from the old.
        """
        words = line.split()
        if len(words)<2:
            raise UIError("Syntax: copy  run_name  new_run_name [new_run_name2 ...]")
        parent_name = words[0]
        child_names = words[1:]
        self.campaign.spawn(parent_name, child_names)

    complete_spawn = _autocomplete_run_name



    def do_postprocess(self, line):
        """
        Postprocess one or more runs
        """
        runs = self.parse_runs(line)
        for run in runs:
            try:
                run.postprocess_synchronously()
            except WrongStatus:
                logging.error("Run %s is not ready to postprocess (%s)", run.name, run.status)
    complete_postprocess = _autocomplete_run_name

    def do_mark(self, line):
        """
        Mark a run as having a different status.
        Syntax: mark status_name  run [run2 ...]
        Where status_name is one of:
            new
            queued
            prepared
            marked_invalid
            running
            waiting
            complete
            error
            superseded
            unknown
            base
        """
        words = line.split()
        if not len(words)>1:
            logging.error("Syntax: mark status_name  run [run2 ...]")
            return
        status_name = "STATUS_" + words[0].upper().strip()
        try:
            status = STATUS_CODES[status_name]
        except KeyError:
            logging.error("Status ({}) must be one of: {}".format(status_name, ", ".join(STATUS_NAMES)))
            return

        for run_name in words[1:]:
            run = self.parse_run(run_name)
            run.status = status

    def do_create(self, line):
        """
        Create one or more new blanks run with no parent.
        """
        words = line.split()
        if len(words)==0:
            raise UIError("Syntax: create run_name1  [run_name2 ...].")
        self.campaign.create(words)

    def complete_create(self, text, line, begidx, endidx):
        return self._autocomplete_path_after_first_arg(text, line, begidx, endidx, "create")

    def do_ingest(self, line):
        """
        Ingest a run, copying it from outside the directory inside as a new parent/umbrella run.
        Syntax: ingest name /path/to/params.ini /path/to/values.ini /path/to/priors.ini "Text describing run"
        """
        try:
            words = shlex.split(line)
            name=words[0]
            params=words[1]
            values=words[2]
            priors=words[3]
            text=words[4]
        except IndexError:
            raise UIError("Syntax: ingest name params.ini values.ini priors.ini description_text")
        self.campaign.ingest(name, params, values, priors, text)

    def complete_ingest(self, text, line, begidx, endidx):
        return self._autocomplete_path_after_first_arg(text, line, begidx, endidx, "ingest")

    def do_shell(self, line):
        """
        Start an ipython shell with local variables "campaign", "runs", and "run_dict".
        """
        from IPython import embed
        campaign=self.campaign
        run_dict=campaign.runs
        runs=campaign.runs.items()
        embed()

    def do_delete(self, line):
        """
        Delete one or more runs, moving them to archive.
        """
        runs = self.parse_runs(line)
        for run in runs:
            self.campaign.delete(run.name)
    complete_delete = _autocomplete_run_name

    do_rm = do_delete
    complete_rm = complete_delete



parser = argparse.ArgumentParser()
parser.add_argument("campaign_directory", help="The campaign directory to use")
if __name__ == '__main__':
    args = parser.parse_args()
    #So that subprocesses inherit this
    ui = CampaignUI(args.campaign_directory)
    ui.cmdloop_with_keyboard_interrupt()
