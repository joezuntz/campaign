from .local import LocalLauncher
from .pbs import PBSLauncher
from .fornax import FornaxLauncher
from .stacpolly import StacpollyLauncher
from .eddie import EddieLauncher
import os
import socket

#Detect what machine we are running on
hostname = socket.gethostname()
if 'fornax' in hostname:
    print "Default Job Launcher is Fornax"
    DefaultLauncher = FornaxLauncher
elif 'head'==hostname:
    DefaultLauncher = StacpollyLauncher
elif 'ecdf.ed.ac.uk' in hostname:
    DefaultLauncher = EddieLauncher
else:
    print "Default Job Launcher is Local"
    DefaultLauncher = LocalLauncher


launcher = DefaultLauncher()
