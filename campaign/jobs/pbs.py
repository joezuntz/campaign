from ..log import NOTE, JOB
from ..names import *
from ..errors import *
from ..status import *
import os
import subprocess
import xml.etree.ElementTree
import logging




class PBSJob(object):
    def __init__(self, name, command_line):
        self.name = name
        self.command_line = command_line
        self.id =  None

    def submit(self):
        submission = subprocess.Popen(self.command_line, shell=True, 
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output, _ = submission.communicate()
        status = submission.returncode
        if status:
            raise LaunchError("Could not run submission command '{}' (return code {}".
                format(self.command_line, status))
        job_id = output.strip().split('.')[0]
        self.id = job_id

    def status(self):
        if self.id is None:
            return None #Job not yet queued

        #Run qstat -x command which gets job status info
        #in XML form
        cmd = "qstat -x {}".format(self.id)
        qstat = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output, _ = qstat.communicate()

        #Error code indicates job is not in the list and is thus complete.
        #Probably
        if qstat.returncode:
            return STATUS_COMPLETE

        root=xml.etree.ElementTree.fromstring(output)
        job_state = root[0].find("job_state").text

        if job_state == "R":
            return STATUS_RUNNING
        elif job_state == "Q":
            return STATUS_QUEUED
        elif job_state == "H":
            return STATUS_WAITING
        elif job_state == "C":
            return STATUS_COMPLETE
        else:
            return STATUS_UNKNOWN

    def poll(self):
        if self.id is None:
            return None

        status = self.status()

        #This is supposed to mirror Subprocess.poll,
        #which returns the exit status of the command 
        #if it is finished or None otherwise
        if status==STATUS_COMPLETE:
            return 0
        else:
            return None


    def wait(self):
        #Not a good idea!
        return None

    def kill(self):
        if self.id is None:
            return
        cmd = "qdel {}".format(self.id)
        qstat = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        _, _ = qstat.communicate()
        





class PBSLauncher(object):
    mpi=True
    def __init__(self, setup, template, cores_per_node):
        self.template = template
        self.setup = setup
        self.cores_per_node = cores_per_node


    def launch(self, working_directory, campaign_dir, name, command_line, exit_file=False, nodes=1, time_hours=48,  **kwargs):
        working_directory = os.path.abspath(working_directory)
        time_hours = int(time_hours)
        cores_per_node = int(kwargs.get("cores_per_node", self.cores_per_node))
        additional_setup = kwargs.get("job_setup", "")
        nodes = int(nodes)
        hours = int(time_hours)
        minutes = int(60*(time_hours-hours))
        log_name = JOB_LOG_FILENAME
        total_cores = nodes * cores_per_node

        if total_cores==1:
            mpirun=""
            mpi_flag=""
        else:
            mpirun="mpirun"
            mpi_flag="--mpi"

        if exit_file:
            exit_cmd = "echo $? > {}".format(EXIT_STATUS_FILENAME)
        else:
            exit_cmd = ""
        script_args = {
            "nodes": nodes,
            "cores_per_node":cores_per_node,
            "hours": hours,
            "minutes": minutes,
            "name": name,
            "campaign_dir": campaign_dir,
            "working_directory": working_directory,
            "log_name": log_name,
            "setup": self.setup,
            "additional_setup" : additional_setup,
            "total_cores": total_cores,
            "command_line": command_line,
            "exit_cmd": exit_cmd,
            "mpirun":mpirun,
            "mpi_flag":mpi_flag,
        }
        script_args.update(kwargs)
        script = self.template.format(**script_args)
        script_filename=os.path.join(working_directory, JOB_LAUNCH_SCRIPT)

        f = open(script_filename, "w")
        f.write(script)
        f.close()

        submit_command = "qsub {}".format(script_filename)

        logging.log(JOB, "Submitting PSB job %s", script_filename)

        job = PBSJob(name, submit_command)
        job.submit()

        return job


