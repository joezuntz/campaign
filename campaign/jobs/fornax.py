from .pbs import PBSLauncher

fornaxTemplate = """
#PBS -l nodes={nodes}:ppn={cores_per_node}
#PBS -l walltime={hours}:{minutes}:00
#PBS -N {name}
#PBS -o log.tmp
#PBS -j oe

export CAMPAIGN_DIR={campaign_dir}
cd {working_directory}
{setup}
#per-job setup commands
{additional_setup}
{mpirun} -n {total_cores} {command_line} {mpi_flag} &> {log_name}
{exit_cmd}
"""
fornaxCoresPerNode = 32
fornaxSetup = "cd ~/cosmosis-with-bootstrap; source setup-cosmosis; cd -"



class FornaxLauncher(PBSLauncher):
    def __init__(self):
        super(FornaxLauncher,self).__init__(fornaxSetup, fornaxTemplate, fornaxCoresPerNode)
