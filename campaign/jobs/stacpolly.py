from .pbs import PBSLauncher

STACPOLLY_TEMPLATE = """
#PBS -l nodes={nodes}:ppn={cores_per_node}
#PBS -l walltime={hours}:{minutes}:00
#PBS -N {name}
#PBS -o log.tmp
#PBS -j oe

export CAMPAIGN_DIR={campaign_dir}

cd {working_directory}

#Overall setup
{setup}

#Per-job setup
{additional_setup}

# Just in case the user's silly setup took us somewhere else
cd {working_directory}

{mpirun} {command_line} {mpi_flag} &> {log_name}
{exit_cmd}
"""

STACPOLLY_CORES_PER_NODE = 32

STACPOLLY_SETUP = "pushd ~/cosmosis; source setup-cosmosis; popd;"



class StacpollyLauncher (PBSLauncher):
    def __init__ (self, **kwargs):
        super (StacpollyLauncher, self)  .  __init__ (STACPOLLY_SETUP,
                                                      STACPOLLY_TEMPLATE,
                                                      STACPOLLY_CORES_PER_NODE)
