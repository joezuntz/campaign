from .pbs import PBSLauncher

eddieTemplate = """
#!/bin/bash
#$ -N {name}
#$ -pe mpi {total_cores}
#$ -cwd
#$ -l h_rt={hours}:{minutes}:00
#$ -o log.tmp
#$ -j yes

#  #$ -l h_vmem=2G

export CAMPAIGN_DIR={campaign_dir}
cd {working_directory}
{setup}
#per-job setup commands
{additional_setup}
{mpirun} -n {total_cores} {command_line} {mpi_flag} &> {log_name}
{exit_cmd}
"""

eddieCoresPerNode = 16
eddieSetup = """
cd /exports/eddie/scratch/jzuntz/cosmosis
source setup-cosmosis
cd -
"""



class EddieLauncher(PBSLauncher):
    def __init__(self):
        super(EddieLauncher,self).__init__(eddieSetup, eddieTemplate, eddieCoresPerNode)
