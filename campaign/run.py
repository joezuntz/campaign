#Standard library modules
import os
import glob
import shutil
import subprocess
import time
import logging
import ConfigParser
import datetime

#Package modules
from .errors import WrongStatus, RunExists
from .params import RunParameters, ParameterFile
from .log import NOTE, logger
from .status import *
from .names import *
from . import utils
from .jobs import launcher


from cosmosis.runtime.config import CosmosisConfigurationError

"""
Ways of creating a run
 - from a yaml file
 - by copying an existing one
 - by loading it from disc

"""

class Run(object):
    def __init__(self, campaign_directory, name, text, parameters):
        self.campaign_directory = campaign_directory
        self.parameters = parameters
        self.name = name
        self.text = text
        if not self.text.endswith("\n"):
            self.text += "\n"
        self._status = STATUS_NEW
        self.postprocess_parameters = []
        self.job = None


    @property
    def parent(self):
        dirname = self.parameters.parent_dir
        return utils.parse_run_directory(dirname)

    @parent.setter
    def parent(self, parent_run):
        self.parameters.parent_dir = parent_run.run_directory(env=True)


    @classmethod
    def read_directory(cls, campaign_directory, run_directory):
        "Read an existing run from a directory"
        path_bits = run_directory.split(os.path.sep)
        name = path_bits[-1]
        text = open(os.path.join(run_directory, NOTES_FILENAME)).read()
        parameters = RunParameters.read(run_directory)
        parent = None # for now we do not allow complex inheritance chains.  May need to fix to do importance sampling

        run = cls(campaign_directory, name, text, parameters)
        run._status = STATUS_PREPARED
        run.read_status()

        return run


    @classmethod
    def from_dict(cls, campaign_directory, name, data, prepare=True):
        "Create a new run from a dictionary"

        text = data.get('text', '')
        parent = data.get('parent')
        
        base = data.get('base')
        if base is not None:
            base_params = base.get('params')
            base_values = base.get('values')
            base_priors = base.get('priors')

            if (base_params is None) or (base_values is None) or (base_priors is None):
                raise ValueError("Must specify base params, values, and priors to ingest")
            
            run = cls.ingest(campaign_directory, name, base_params, base_values, base_priors, text, prepare=False)
        else:
            parameters = RunParameters.from_dict(data)
            run = cls(campaign_directory, name, text, parameters)
            run.status = data.get('status', STATUS_NEW)

        run.postprocess_parameters = data.get('postprocess_parameters', [])

        if prepare:
            run.prepare()
        return run, parent

    @classmethod
    def ingest(cls, campaign_directory, name, params, values, priors, text="", prepare=True):
        def make(p):
            if p=="--":
                return ParameterFile(None)
            else:
                return ParameterFile.read(p)

        params = make(params)
        values = make(values)
        priors = make(priors)
        parameters = RunParameters(params, values, priors)
        parameters.set_paths()
        run = cls(campaign_directory, name, text, parameters)
        run._status = STATUS_BASE
        if prepare:
            run.prepare()
        return run


    @classmethod
    def create_blank(cls, campaign_directory, name, parent=None):
        if parent is None:
            data = {}
        else:
            data = {
                "params":{"base_path":parent.params_path(env=True)},
                "values":{"base_path":parent.values_path(env=True)},
                "priors":{"base_path":parent.priors_path(env=True)},
            }
        run, parent = cls.from_dict(campaign_directory, name, data)
        return run

    def spawn(self, name):
        child = Run.create_blank(self.campaign_directory, name, self)
        return child

    def copy(self, name, text=None):
        if text is None:
            text = self.text
        run = self.__class__(self.campaign_directory, name, 
            self.text, self.parameters.copy())
        run.postprocess_parameters = self.postprocess_parameters[:]
        run.prepare()
        return run


    def status():
        doc = "The status property."
        def fget(self):
            return self._status
        def fset(self, value):
            if value!=STATUS_NEW:
                if not os.path.exists(self.run_directory()):
                    utils.mkdir(self.run_directory())
                open(self.status_filename(), 'w').write("%s\n"%value)
            self._status = value
        def fdel(self):
            del self._status
        return locals()
    status = property(**status())

    def synchronize(self):
        self.read_status()
        self.text = open(os.path.join(self.run_directory(), NOTES_FILENAME)).read()
        self.parameters = RunParameters.read(self.run_directory())

    def read_status(self):
        if os.path.exists(self.status_filename()):
            self._status = open(self.status_filename()).read().strip()




    def __str__(self):
        return "<Run {}>".format(self.name)

    def __repr__(self):
        return self.__str__()

    def same_parameters(self, other):
        return self.parameters == other.parameters

    def needs_rerun(self, other):
        assert self.name == other.name
        if not self.same_parameters(other):
            logger.log(logging.DEBUG, "Run %s has changed because parameters have changed.", self)
            return True
        return False

    def edit(self, filename):
        if self.status not in [STATUS_PREPARED,STATUS_BASE]:
            raise WrongStatus(self.status)
        run_dir = self.run_directory()
        path = os.path.join(run_dir, filename)
        if not os.path.exists(path):
            raise ValueError("No file {} in run {}. ({})".format(filename, self.name, path))
        cmd = "emacs -nw {}".format(path)
        logger.log(NOTE, cmd)
        os.system(cmd)



    def prepare(self):
        #check status. can only prepare new runs, not ones
        #that are already complete
        if self.status not in [STATUS_NEW, STATUS_BASE]:
            raise WrongStatus(self.status)

        directory = self.run_directory()
        if os.path.exists(directory):
            raise RunExists(directory)

        logger.log(NOTE, "Preparing run %s in directory %s", 
            self.name, directory)

        utils.mkdir(directory)

        #save all parameter files to this directory
        self.parameters.write(directory)

        f = open(os.path.join(directory, NOTES_FILENAME), 'w')
        f.write(self.text)
        f.close()
        
        #Update status
        if self.status==STATUS_NEW:
            self.status = STATUS_PREPARED
        else:
            self.status = STATUS_BASE
        
        #create launch script there too?

    def unprepare(self):
        if self.status != STATUS_PREPARED:
            raise WrongStatus(self.status)

        shutil.rmtree(self.run_directory())
        self.status = STATUS_NEW

    def get_param(self, section, name):
        return self.parameters.params[section, name]
    def set_param(self, section, name, value):
        self.parameters.params[section, name] = value
    def del_param(self, section, name):
        del self.parameters.params[section, name]

    def get_value(self, section, name):
        return self.parameters.values[section, name]
    def set_value(self, section, name, value):
        self.parameters.values[section, name] = value
    def del_value(self, section, name):
        del self.parameters.values[section, name]

    def get_prior(self, section, name):
        return self.parameters.priors[section, name]
    def set_prior(self, section, name, value):
        self.parameters.priors[section, name] = value
    def del_prior(self, section, name):
        del self.parameters.priors[section, name]
    def get_sampler(self):
        sampler=self.parameters.params['runtime', 'sampler'] 
        return sampler
    def set_sampler(self, sampler):
        self.parameters.params['runtime', 'sampler'] = sampler

    def supersede(self):
        if self.status == STATUS_MARKED_INVALID:
            logger.log(NOTE, "%s is superseded but it is already marked invalid so leaving its status alone",
             self.name)
        else:
            logger.log(NOTE, "Marking %s as superseded", self.name)
            self.status = STATUS_SUPERSEDED

    def run_directory(self, env=False):
        if env:
            return os.path.join("${CAMPAIGN_DIR}/", RUNS_SUBDIR, self.name)
        else:
            return os.path.join(self.campaign_directory, RUNS_SUBDIR, self.name)

    def path(self, filename, env=False):
        run_dir = self.run_directory()
        if env:
            assert run_dir.startswith(self.campaign_directory)
            l = len(self.campaign_directory)
            run_dir = "${CAMPAIGN_DIR}/"+run_dir[l:]
        return os.path.join(run_dir, filename)

    def chain_path(self, env=False):
        return self.path(CHAIN_FILENAME, env=env)

    def output_directory(self, env=False):
        return self.path(OUTPUTS_SUBDIR, env=env)

    def test_directory(self, env=False):
        return self.path(TEST_SUBDIR, env=env)

    def status_filename(self, env=False):
        return self.path(STATUS_FILENAME, env=env)

    def log_filename(self, env=False):
        return self.path(JOB_LOG_FILENAME, env=env)

    def params_path(self, env=False):
        return self.path(PARAM_FILENAME, env=env)

    def values_path(self, env=False):
        return self.path(VALUES_FILENAME, env=env)

    def priors_path(self, env=False):
        return self.path(PRIORS_FILENAME, env=env)

    def archive(self, directory, comment=""):
        #kill an ongoing run and change the status to invalid
        self.invalidate()
        #Move to the archive dir
        logger.log(NOTE, "Archiving run {} to {}".format(self.name, directory))
        shutil.move(self.run_directory(), directory)
        #Record the time and possibly a comment in the archive directory
        archive_date = datetime.datetime.now().isoformat()
        text = archive_date+"\n"
        if comment:
            text += comment+"\n"
        open(os.path.join(directory, "archive.txt"),"w").write(text)

    def run_command(self):
        command = [
            "cosmosis", PARAM_FILENAME,
        ]
        return command

    def _run_process(self, command):
        directory = self.run_directory()
        logger.log(NOTE, "Running command:\n     %s\nin directory:\n    %s", ' '.join(command), directory)
        process = subprocess.Popen(command, stdout=subprocess.PIPE, 
            stderr=subprocess.STDOUT, cwd=directory)
        return process

    def test_synchronously(self, pipeline_test=False):
        original_status = self.status
        #First, check if we are already set to use the test
        #sampler. If so, and this is just the pipeline test
        #at the start of a run, then we don't need to run the
        #test twice.
        sampler = self.get_sampler()
        if pipeline_test and sampler=="test":
            return 0, ""

        #temporarily set the sampler to the test sampler and run
        #We don't tell it to test_first as that would be an infinite
        #recursion.

        #Overwrite the sampler
        command = self.run_command() + ["-p", "runtime.sampler=test", "pipeline.timing=T", "pipeline.debug=T", "pipeline.quiet=F"]
        process=self._run_process(command)

        output,_ = process.communicate()
        return_code = process.returncode

        print output

        if return_code:
            logger.error("Pipeline test failed")
        else:
            logger.error("Pipeline test succeeded")


        #Reset to previous status
        self.status = original_status

        return return_code, output


    def postprocess_synchronously(self, parameters={}, force=False):
        #We usually only want to postprocess complete commands,
        #but occasionally we might want to do it in the middle of 
        #a run in case things partially finished
        if self.status!=STATUS_COMPLETE and not force:
            raise WrongStatus(self.status)

        #Get the command to run, pasing any extra postprocessing
        #requirements in.
        command = self.postprocess_command(parameters)

        #Set up the process and run it
        process=self._run_process(command)

        output,_ = process.communicate()
        return_code = process.returncode

        return return_code, output

    def output_files(self):
        return glob.glob(os.path.join(self.output_directory(), "*"))

    def test_files(self):
        return glob.glob(os.path.join(self.test_directory(), "*"))

    def postprocess_command(self, parameters=[]):
        #check if status is finished
        #if not, raise error, unless Force is True

        #Global postprocess parameters and specific ones for this
        #run (might tweak, e.g. burn-in, etc.)
        #should be dictionaries of "--burn XXX" etc.
        parameters = self.postprocess_parameters + parameters
        
        #construct the command that runs postprocess.
        command = ["postprocess", "-o", OUTPUTS_SUBDIR, PARAM_FILENAME] + parameters
        return command

    def launch(self, test_first=True):
        if test_first:
            #Check that the pipeline is working first
            test_code, test_output = self.test_synchronously(pipeline_test=True)

            #If the test fails 
            if test_code:
                self.status=STATUS_ERROR
                logger.error("Pre-launch test failed. Output below")
                print test_output
                logger.error("Pre-launch test failed. Output above")
                return


        if self.status == STATUS_PREPARED:
            working_directory = self.run_directory()
            command_line = " ".join(self.run_command())
            try:
                launch_parameters = self.parameters.params.items("campaign")
            except ConfigParser.NoSectionError:
                launch_parameters = {}
            logger.log(logging.DEBUG, "Launcher parameters for job: %s",launch_parameters)
            self.job = launcher.launch(working_directory, self.campaign_directory, self.name, command_line, exit_file=True, **launch_parameters)
            self.status = STATUS_QUEUED
        else:
            raise WrongStatus(self.status)

    def invalidate(self):
        if self.job is not None:
            self.job.kill()
            self.job = None
        self.status = STATUS_MARKED_INVALID

    def exit_status(self):
        exit_filename = os.path.join(self.run_directory(), EXIT_STATUS_FILENAME)
        status = None
        if os.path.exists(exit_filename):
            status = open(exit_filename).read().strip()
            if status=="0":
                status = STATUS_COMPLETE
            else:
                status = STATUS_ERROR
        return status


    def update(self):
        if self.job is not None:
            status = self.job.status()
            if status == STATUS_COMPLETE:
                #the job status being complete just tells us that the
                #the job finished, not that it finished successfully
                status = self.exit_status()

                #If we are complete then we can delete the job
                self.job = None

        #Or we may not have a job at all.
        #Either we have never run at all or we have run and finished
        #earlier
        else:
            if self.status == STATUS_COMPLETE:
                status = self.exit_status()
            else:
                status = None
            #If this is None then the job has not run at all - status 
            #should be STATUS_NEW, so no changes
            if status is None:
                #If we have no exit status file telling us the output
                #then the job has not run and we have no new updates
                return
        if status != self.status:
            self.status = status

    def stop(self):
        "Stop a run. It will be marked complete if there is any chain output"
        if self.job is None:
            logger.warning("Job {} is not running.".format(self.name))
            return
        self.job.kill()
        if self.line_count <= 0:
            self.status = STATUS_ERROR
        else:
            self.status = STATUS_COMPLETE




    def detail(self):
        table = [
            ("Name", self.name),
            ("Info", self.text),
            ("Status", STATUS_ICON[self.status] + " " + self.status),
            ("Parent", ','.join(self.parent)),
            ("Base Params", self.parameters.params.base_path), 
            ("Base Values", self.parameters.values.base_path), 
            ("Base Priors", self.parameters.priors.base_path), 
        ]
        for name,p in [
            ("Params", self.parameters.params),
            ("Values", self.parameters.values), 
            ("Priors", self.parameters.priors),]:
            if p.changes:
                for i,line in enumerate(utils.changes_text(p.changes)):
                    line = line[:50]
                    if len(line)==50:
                        line += " ... "
                    if i==0:
                        table.append((name, line))
                    else:
                        table.append(("", line))
            else:
                table.append((name, ""))
        return table  

    def line_count(self):
        chain = self.chain_path()
        if not os.path.exists(chain):
            return -1
        file_size = os.stat(chain).st_size
        if file_size<5000000:
            lc = self.exact_line_count()
            return "{}".format(lc)
        else:
            lc = self.approx_line_count()
            return "~ {}".format(lc)

    def approx_line_count(self):
        #fast approximate line count variant
        chain = self.chain_path()
        if not os.path.exists(chain):
            return -1
        line_length=0
        lines = utils.tail(chain,20)
        for line in lines:
            if line.startswith('#'):
                continue
            line_length = len(line)

        if line_length==0:
            return 0
        #Get total length of comment lines at the top
        comment_bytes = 0
        for line in open(chain):
            if not line.startswith('#'):
                break
            comment_bytes += len(line)

        file_size = os.stat(chain).st_size
        approx_lines = (file_size-comment_bytes)//line_length
        return approx_lines


    def exact_line_count(self):
        chain = self.chain_path()
        if os.path.exists(chain):
            lines = utils.uncommented_line_count(chain)
        else:
            lines = -1
        return lines        

    def info(self):
        self.update()
        lines = self.line_count()
        try:
            sampler = self.get_sampler()
        except CosmosisConfigurationError:
            sampler = "?"

        job = getattr(self.job, 'id', '')

        outputs = self.output_files()
        n_output = len(outputs)
        return (self.name, self.parent, self.status, sampler, lines, n_output, job)
    #Update if we update info method:
    INFO_FIELDS = ("Name", "Parent", "Status", "Sampler", "Lines", "Outputs", "Job")
