from pyramid.view import view_config
from pyramid.response import FileResponse
from socket import gethostname
from campaign import errors
import fnmatch
import os



@view_config(route_name='home', renderer='home.html.pt')
def home_view (request):
    """This function is called by the pyramid framework whenever the browser
       asks for the `root' URL (index.html in the traditional sense).  The
       effect is to do the standard browser thing: locate the HTML file (and
       maybe fill in some template parameters in our case) and send the
       resulting contents back to the browser."""

    return {"hostname" : gethostname ()}



@view_config(route_name='provide-text', renderer='json')
def provide_text (request):
    """Called by the pyramid framework when a browser requests the contents
       of a .ini file to put into the in-form text editor.  We simply open
       the file, put its contents verbatim into the response object, and
       return this.

       But the request may also contain an updated text to be written back
       to the server; we deal with this first."""

    req = request.json_body

    if 'new-file' in req:
        open ("a/runs/" + req ['run-name'] + '/' + str(req ['version'])
                        + '/' + req ['new-file'],
              'w') . write (req ['new-content'])

    if ('file' in req
           and  ('new-file' not in req   or  req ['new-file'] != req ['file'])):
        return {"new-content" : True,
                "content" : open ('a/runs/' + req ['run-name'] + '/'
                                        + str(req ['version']) + '/'
                                        + req ['file']) . read ()}

    else:
        return {"new-content" : False}
    


@view_config(route_name='get-campaign-state', renderer='json')
def get_campaign_state (request):
    """This function is called by the pyramid framework when the browser
       makes an AJAX request for the current state of the server; the
       request may also contain an instruction to effect a change to the
       state of the server.

       We obtain the Campaign object which actually holds the current
       state of the object through the pyramid framework (it was created
       and put there in the __init__ function).  We then look to see if
       any state-changing action is asked for in the request, and invoke
       the corresponding method in the Campaign object to enact this.

       Finally, we reap all of the state information into a single structure,
       and send this back to the browser as the AJAX response.  Note that,
       in order to ease merging new data into existing data in the browser,
       this returned object is flat with the structure of the information
       relayed via the naming scheme of the data members."""

    # The global campaign object.
    c = request.registry.settings ['campaign']


    # Process any action which the request from the browser may be asking for.
    try:

        if request.json_body != None:

            name = request.json_body ['name']

            try:
                ({
                  'reset':        lambda: c.runs [name].reset (),
                  'prepare':      lambda: c.runs [name].prepare (),
                  'submit':       lambda: c.launch (name),
                  'postprocess':  lambda: c.runs [name]
                                                 . postprocess_synchronously (),
                  'next-version': lambda: c.next_version (name),
                  'copy':         lambda: c.copy (name,  name + "__1"),
                  'drop-run':     lambda: c.drop_run (name),
                  'rename-run':   lambda: c.rename_run (
                                           name, request.json_body ['new-name'])
                }
                [request.json_body ['action']]) ()

            except errors.WrongStatus:
                # !!!! Do something better here.
                pass
                                 
    except ValueError:
        # Maybe there was not an `action' item there at all... just do nothing
        # in that case.
        pass

 

    # The return object, a structure which is the response to the AJAX request.
    ret = []

    for i,run in enumerate (c.runs.values ()):

        (name, version, status, sampler, lines, n_outputs, job) = run.info ()

        if lines == -1:
            lines = ''

        item = {'index': i, 'name': name, 'version': version,
                'information': run.text, 'parents': ','.join (run.parent),
                'base-parameters': run.parameters.params.base_path,
                'base-values': run.parameters.values.base_path,
                'base-priors': run.parameters.priors.base_path,
                'status': status, 'sampler': sampler,
                'chain-length': lines, 'n-outputs': n_outputs,
                'job': job}

        if n_outputs > 1:
            item ['outputs'] = []
            for dir, sub_dirs, files in (
                                  os.walk (c.runs [name].output_directory ())):
                item ['outputs'] += (fnmatch.filter (files, '*.png'))

        out_dir = 'a/runs/' + name + '/' + str(version)

        item ['inputs'] = []
        for dir, sub_dirs, files in (os.walk (out_dir)):
            item ['inputs'] += (fnmatch.filter (files, '*.ini'))

        item ['test-outputs'] = []
        try:
            for dir in os.listdir (out_dir + '/test'):
                for file in os.listdir (out_dir + '/test/' + dir):
                    item ['test-outputs'] . append (dir + '/' + file)
        except OSError:
            # Maybe the test directory is not there yet, so just carry on.
            pass
            

        ret.append (item)

    return ret



@view_config(route_name='get-png')
def get_png (request):
    """This function is called by the pyramid framework when the browser
       asks for an output PNG image to be returned.  We get the Campaign object
       through the pyramid framework in order to determine the directory which
       will contain this image, and then return it to the browser again
       through the pyramid framework."""

    c = request.registry.settings ['campaign']

    return FileResponse (c.runs [request.GET ['campaign']].output_directory ()
                             + '/'
                             + request.GET ['png'],
                         request = request,
                         content_type = 'image/png')
